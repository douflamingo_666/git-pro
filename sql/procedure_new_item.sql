DROP PROCEDURE IF EXISTS new_item;
DELIMITER $
CREATE PROCEDURE new_item()
BEGIN
    set @i=1;
    WHILE @i<=1000000 DO
    INSERT INTO `mysql_learning`.`bb_item_3_partition` (`id`, `display_item_id`, `parent_item_id`, `form`, `unique_id`, `type`, `question_time`, `product_id`, `link_product_id`, `parent_catgory_id`, `category_id`, `flow_id`, `step_id`, `step_type_id`, `real_route`, `route`, `title`, `account`, `account_credit_lv`, `account_vip_flag`, `area`, `area_cn`, `server`, `server_cn`, `role_name`, `contact_num`, `call_num`, `email`, `id_type`, `id_num`, `realname`, `is_important`, `is_vip_question`, `bind_mobile_num`, `bind_call_num`, `bind_sfz`, `step_exp_time`, `step_notice_time`, `create_user_group_id`, `create_user_id`, `user_group_id`, `user_id`, `attention_counts`, `append_msg_counts`, `msg_counts`, `call_counts`, `create_status`, `item_status`, `group_status`, `status`, `process_user`, `notice_status`, `inputtime`, `updatetime`) VALUES (@i, concat('0012719060600', @i) , '0', rand_form(round(rand()*4)), '0', rand_type(round(rand()*2)), '2019-06-03 00:00:00|2019-06-03 23:59:59', round(rand()*10+1), '0', round(rand()*50+10), round(rand()*100+1), '0', round(rand()*30+1), round(rand()*40+1), '1', '1', rand_string(20, 3), concat(@i, rand_string(10, 1)), '0', '0', '9', concat('电信', rand_string(1, 6), '区'), round(rand()*30+1), rand_string(7, 5), '炎清歌', rand_string(11, 0), '未填写', '0', '0', '0', '234', '0', '0', '0', '0', '0', '2999999999', '2999999999', '0', round(rand()*30+1), '0', '0', '0', '0', '0', '0', round(rand()*10), round(rand()*2), '0', rand_status(round(rand()*4)), '0', '0', '1559806397', now());

        SET @i=@i+1;
    END WHILE;
END $
CALL new_item()$
DELIMITER ;
DROP PROCEDURE new_item;